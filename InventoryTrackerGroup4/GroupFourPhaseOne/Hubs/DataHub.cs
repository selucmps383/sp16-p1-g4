﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR.Hubs;
using System.Collections.Concurrent;
using System.Data.SqlClient;
using GroupFourPhaseOne.Models;

namespace GroupFourPhaseOne.SignalRHubs {
    public class DataHub : Hub {
 
        public void GrabInventory() {
            using (var context = new InventoryTrackerDatabase()) {
                var names = from n in context.InventoryItems
                            select n.Name;
                var quantities = from q in context.InventoryItems
                                 select q.Quantity;

                Clients.Caller.ItemsGrabbed(names , quantities);
            }
        }
    }
}