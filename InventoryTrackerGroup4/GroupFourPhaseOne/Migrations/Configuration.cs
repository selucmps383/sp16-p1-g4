namespace InventoryTracker_GroupFour.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using GroupFourPhaseOne.Models;
    using System.Web.Helpers;
    internal sealed class Configuration : DbMigrationsConfiguration<GroupFourPhaseOne.Models.InventoryTrackerDatabase>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(GroupFourPhaseOne.Models.InventoryTrackerDatabase context)
        {
            context.InventoryItems.AddOrUpdate(i=>i.Name, 
                new InventoryItem { Name = "Heater",CreatedByUserId=1, Quantity = 35 },
                new InventoryItem { Name = "Lamp", CreatedByUserId = 1, Quantity = 14 },
                new InventoryItem() { Name = "Toaster", CreatedByUserId = 2, Quantity = 35 },
                new InventoryItem() { Name = "Freezer", CreatedByUserId = 1, Quantity = 88 },
                new InventoryItem() { Name = "Microwave", CreatedByUserId = 2, Quantity = 75 },
                new InventoryItem() { Name = "Widget", CreatedByUserId = 3, Quantity = 7 },
                new InventoryItem() { Name = "English Muffin", CreatedByUserId = 4, Quantity = 13 },
                new InventoryItem() { Name = "Keyboard", CreatedByUserId = 2, Quantity = 356 },
                new InventoryItem() { Name = "Stopwatch", CreatedByUserId = 1, Quantity = 13 },
                new InventoryItem() { Name = "Oven", CreatedByUserId = 3, Quantity = 24 },
                new InventoryItem() { Name = "Stove", CreatedByUserId = 2, Quantity = 57 });

            context.Users.AddOrUpdate(u => u.Username,
                new User { Username = "admin", FirstName = "Mister", LastName = "Administrator", Password = Crypto.HashPassword("selu2014") },
                new User { Username = "mColeman", FirstName = "Michelle", LastName = "Coleman", Password = Crypto.HashPassword("selu2014") },
                new User { Username = "jTulk", FirstName = "Jay", LastName = "Tulk", Password = Crypto.HashPassword("selu2014") },
                new User { Username = "cCarter", FirstName = "Caleb", LastName = "Carter", Password = Crypto.HashPassword("selu2014") },
                new User { Username = "nThompson", FirstName = "Neil", LastName = "Thompson", Password = Crypto.HashPassword("selu2014") },
                new User { Username = "kCarpenter", FirstName = "Kevin", LastName = "Carpenter", Password = Crypto.HashPassword("selu2014") },
                new User { Username = "lTester", FirstName = "Lester", LastName = "Tester", Password = Crypto.HashPassword("selu2014") });
        }
    }
}
