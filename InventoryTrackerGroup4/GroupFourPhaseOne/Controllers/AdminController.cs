﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GroupFourPhaseOne.Controllers
{   // This is the AdminController it returns the views specific to the admin pages such as adminhome and adminIndex.
    // This page is authorized and may only be viewed by users that have logged in.

    [Authorize]
    public class AdminController : Controller
    {
        // GET: Admin
        public ActionResult AdminIndex()
        {
            return View();
        }

        public ActionResult AdminHome()
        {
            return View();
        }
    }
}