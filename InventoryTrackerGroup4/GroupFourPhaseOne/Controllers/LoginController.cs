﻿using GroupFourPhaseOne.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.Helpers;


// Creates a cookie to authorize users
// References logout
// Compares login credentials to what is in the database
// Verifies Crypto.HashPasswords

namespace GroupFourPhaseOne.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult LogIn()
        {
            return View();
        }

        [HttpPost]

        public ActionResult LogIn(User user)

        {

            //if (ModelState.IsValid)

            //{

            if (IsValid(user.Username, user.Password))

            {

                FormsAuthentication.SetAuthCookie(user.Username, false);

                return RedirectToAction("AdminHome", "Admin");

            }

            else

            {

                ModelState.AddModelError("", "Username or Password is incorrect.");

            }
            return View(user);

        }



        public ActionResult LogOut()

        {

            FormsAuthentication.SignOut();

            return RedirectToAction("Index", "Home");

        }

        private bool IsValid(string Username, string Password)

        {


            bool IsValid = false;



            using (var db = new InventoryTrackerDatabase())

            {

                var user = db.Users.FirstOrDefault(u => u.Username == Username);

                if (user != null)

                {

                    if (Crypto.VerifyHashedPassword(user.Password, Password))

                    {

                        IsValid = true;

                    }

                }

            }

            return IsValid;

        }
    }
}


        