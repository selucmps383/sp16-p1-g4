﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using GroupFourPhaseOne.Models;

namespace GroupFourPhaseOne.Controllers.Api
{
    public class InventoryItemController : ApiController
    {
        private InventoryTrackerDatabase db = new InventoryTrackerDatabase();

        // GET: api/InventoryItem
        public IQueryable<InventoryItem> GetInventoryItems()
        {
            return db.InventoryItems;
        }

        // GET: api/InventoryItem/5
        [ResponseType(typeof(InventoryItem))]
        public async Task<IHttpActionResult> GetInventoryItem(int id)
        {
            InventoryItem inventoryItem = await db.InventoryItems.FindAsync(id);
            if (inventoryItem == null)
            {
                return NotFound();
            }

            return Ok(inventoryItem);
        }

        // PUT: api/InventoryItem/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutInventoryItem(int id, InventoryItem inventoryItem)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != inventoryItem.Id)
            {
                return BadRequest();
            }

            db.Entry(inventoryItem).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!InventoryItemExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/InventoryItem
        [ResponseType(typeof(InventoryItem))]
        public async Task<IHttpActionResult> PostInventoryItem(InventoryItem inventoryItem)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.InventoryItems.Add(inventoryItem);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = inventoryItem.Id }, inventoryItem);
        }

        // DELETE: api/InventoryItem/5
        [ResponseType(typeof(InventoryItem))]
        public async Task<IHttpActionResult> DeleteInventoryItem(int id)
        {
            InventoryItem inventoryItem = await db.InventoryItems.FindAsync(id);
            if (inventoryItem == null)
            {
                return NotFound();
            }

            db.InventoryItems.Remove(inventoryItem);
            await db.SaveChangesAsync();

            return Ok(inventoryItem);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool InventoryItemExists(int id)
        {
            return db.InventoryItems.Count(e => e.Id == id) > 0;
        }
    }
}