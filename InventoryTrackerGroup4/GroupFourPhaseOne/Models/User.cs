﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Helpers;
namespace GroupFourPhaseOne.Models
{
    public class User
    {
        public User()
        {

        }
        public User(string pass)
        { 
            Password = Crypto.HashPassword(pass);
        }
        public virtual int Id { get; set; }
        public virtual string Username { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
        public virtual string Password { get; set; }
    }
}
