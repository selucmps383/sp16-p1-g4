﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace GroupFourPhaseOne.Models
{
    public class InventoryTrackerDatabase:DbContext
    {

        public InventoryTrackerDatabase(): base("DefaultConnection")
        {

        }
        public DbSet<InventoryItem> InventoryItems { get; set; }
        public DbSet<User> Users { get; set; }
    }
}
