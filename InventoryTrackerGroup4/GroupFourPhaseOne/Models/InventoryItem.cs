﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GroupFourPhaseOne.Models
{
    public class InventoryItem
    {
        public InventoryItem()
        {

        }
        public InventoryItem(int userId)
        {
            CreatedByUserId = userId;
        }
        public virtual int Id { get; set; }
        public virtual int CreatedByUserId { get; set; }
        public virtual string Name { get; set; }
        public virtual int Quantity { get; set; }
    }
}
